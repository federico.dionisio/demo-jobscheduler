package com.example.demo.scheduler;

import java.util.logging.Logger;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class JobScheduler {
	
	private static Logger LOGGER  = Logger.getLogger(JobScheduler.class.getName());
	
	@Scheduled(fixedRate = 1000)
	//@Scheduled(fixedRateString = "${fixedRate.in.milliseconds}")
	public void scheduleUsingRate() {
		LOGGER.info("Fixed rate task - " + System.currentTimeMillis() / 1000);
	}
	
	@Scheduled(fixedDelay = 5000)
	//@Scheduled(fixedDelayString = "${fixedDelay.in.milliseconds}")
	public void scheduleUsingDelay() {
		LOGGER.info("Fixed delay task - " + System.currentTimeMillis() / 1000);
	}
	
	@Scheduled(fixedDelay = 1000, initialDelay = 1000)
	public void scheduleFixedRateWithInitialDelayTask() {
	    long now = System.currentTimeMillis() / 1000;
	    LOGGER.info("Fixed rate task with one second initial delay - " + now);
	}
	
	@Scheduled(cron = "0 15 10 15 * ?")
	//@Scheduled(cron = "${cron.expression}")
	public void scheduleTaskUsingCronExpression() {
	    long now = System.currentTimeMillis() / 1000;
	    LOGGER.info("schedule tasks using cron jobs - " + now);
	}
	
	
}
